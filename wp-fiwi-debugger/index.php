<?php

/*

Plugin Name: FIWI Debugger
Description: Debug tool for FIWI sites. Press CTRL + D to launch.
Version: 1.1
Author: Findsome & Winmore
Author URI: https://findsomewinmore.com/
Text Domain: fiwidebugger

*/
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
  'https://bitbucket.org/findsomewinmore/fiwi-wp-debugger/',
  __FILE__,
  'fiwi-wp-debugger'
);

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

function fiwi_debugger() {
    echo '<script type="text/javascript" src="https://s3.amazonaws.com/fw-devtools/fiwi-debugger/fiwidebugger.min.js"></script>';
}
add_action( 'wp_footer', 'fiwi_debugger', 100 );


